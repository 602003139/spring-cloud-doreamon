package cn.shawadika.wechat.service;

import cn.shawadika.common.bean.message.WechatTextMessage;
import cn.shawadika.common.dto.WechatBatchOpenidDTO;
import cn.shawadika.common.dto.WechatFansInfoDTO;
import cn.shawadika.common.dto.WechatFansInfoListDTO;
import cn.shawadika.common.dto.WechatOpenidDTO;
import cn.shawadika.common.repo.wechat.*;
import cn.shawadika.common.util.CharUtil;
import cn.shawadika.common.util.Iterables;
import cn.shawadika.wechat.repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Service
public class WechatService {

    @Autowired
    private WechatTokenRepo wechatTokenRepo;
    @Autowired
    private WechatBatchOpenidRepo wechatBatchOpenidRepo;
    @Autowired
    private WechatOpenidRepo wechatOpenidRepo;
    @Autowired
    private WechatFansInfoRepo wechatFansInfoRepo;
    @Autowired
    private GamedailyRepo gamedailyRepo;
    @Autowired
    private WechatUtils wechatUtils;
    @Autowired
    private WechatMessageRepo wechatMessageRepo;

    public GamedailyEntity findGamedailyById(long gamedailyId){
        return gamedailyRepo.findOne(gamedailyId);
    }

    /**
     * 保存token
     *
     * @param token     token
     * @param expiresIn 有效时间（秒）
     */
    public WechatTokenEntity buildToken(String token, int expiresIn) {
        long currentTimeMillis = System.currentTimeMillis();
        long overdueTimeMillis = currentTimeMillis + expiresIn * 1000;
        WechatTokenEntity tokenEntity = WechatTokenEntity.builder()
                .token(token)
                .createTime(new Timestamp(currentTimeMillis))
                .overdueTime(new Timestamp(overdueTimeMillis))
                .build();
        return wechatTokenRepo.save(tokenEntity);
    }

    /**
     * 保存批次openid
     *
     * @param gamedaily   该批次所属的微信公众号id
     * @param count      该批次需要获取条数
     * @param total      实际获取到总条数
     * @param nextOpenid 最后一条openid
     * @return WechatAcceptBatchOpenidEntity
     */
    public WechatBatchOpenidEntity saveOpenidBatch(long gamedaily,int count, int total, String nextOpenid) {
        WechatBatchOpenidEntity batchOpenidEntity = WechatBatchOpenidEntity.builder()
                .count(count)
                .createTime(new Timestamp(System.currentTimeMillis()))
                .nextOpenid(nextOpenid)
                .total(total)
                .gamedailyId(gamedaily)
                .build();
        return wechatBatchOpenidRepo.save(batchOpenidEntity);
    }

    /**
     * 获取一个批次的openid并保存
     *
     * @param nextOpenid 从这个openid开始获取
     * @return
     */
    @Transactional
    public void buildWechatOpenidAndBatch(String nextOpenid,long gamedailyId) {
        WechatBatchOpenidDTO openidBatch = wechatUtils.getOpenidBatch(nextOpenid);
       this.saveBatchAndOpenid(openidBatch,gamedailyId);
    }

    @Transactional
    public void buildWechatOpenidAndBatch(WechatBatchOpenidDTO wechatBatchOpenidDTO,long gamedailyId) {
        this.saveBatchAndOpenid(wechatBatchOpenidDTO,gamedailyId);
    }

    private void saveBatchAndOpenid(WechatBatchOpenidDTO openidBatch,long gamedailyId){
        WechatBatchOpenidEntity wechatBatchOpenidEntity = this.saveOpenidBatch(gamedailyId,openidBatch.getCount(), openidBatch.getTotal(), openidBatch.getNextOpenid());
        WechatOpenidDTO openidList = openidBatch.getData();
        if(openidList == null){
            return;
        }
        List<String> openids = openidList.getOpenid();
        //TODO (2017-12-7)这种保存可能会有问题，目前我还不知道更好的做法
        openids.forEach(w -> {
            WechatOpenidEntity entity = new WechatOpenidEntity();
            entity.setOpenid(w);
            entity.setWechatBatchOpenidId(wechatBatchOpenidEntity.getId());
            wechatOpenidRepo.save(entity);
        });
    }

    /**
     * 获取某个批次的openid
     *
     * @param batchId 批次id
     * @return WechatBatchOpenidEntity
     */
    public WechatBatchOpenidEntity getWechatBatchOpenid(long batchId) {
        return wechatBatchOpenidRepo.findOne(batchId);
    }

    /**
     * 获取最新批次的微信粉丝openid
     */
    public WechatBatchOpenidEntity getLastedBatchOpenid() {
        //TODO (2017-12-7) 现在我还不知道怎么用JPA查询id最大的纪录
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        List<WechatBatchOpenidEntity> batchOpenidEntities = wechatBatchOpenidRepo.findAll(sort);
        WechatBatchOpenidEntity wechatBatchOpenidEntity = null;
        if(batchOpenidEntities.size()>0){
            wechatBatchOpenidEntity=batchOpenidEntities.get(0);
        }
        return wechatBatchOpenidEntity;
    }

    /**
     * 保存openid
     *
     * @param openid  openid
     * @param batchId 该批次id
     * @return WechatOpenidEntity
     */
    public WechatOpenidEntity buildOpenid(String openid, long batchId) {
        WechatOpenidEntity openidEntity = WechatOpenidEntity.builder()
                .openid(openid)
                .wechatBatchOpenidId(batchId).build();
        return wechatOpenidRepo.save(openidEntity);
    }

    /**
     * 批量保存粉丝信息
     *每次最多可以获取100个粉丝信息
     * @param entityList
     */
    public void batchSaveWechatFansInfo(List<WechatFansInfoEntity> entityList,long gamedailyId) {
        //TODO 这里会频繁操作数据库（后期需要优化）
        //TODO (2017-12-7)这种保存可能会有问题，目前我还不知道更好的做法
        Iterables.forEach(entityList,(index,infoEntity)->{
            infoEntity.setGamedailyId(gamedailyId);
            //过滤掉emoji表情，不然插入数据库会报错
            infoEntity.setNickname(CharUtil.filterEmoji(infoEntity.getNickname()));
            //先判断这个粉丝在该公众号是否是否已经保存过，如果已经存在执行更新操作
            WechatFansInfoEntity fansInfoEntity = this.findByOpenidAndGamedailyId(infoEntity.getOpenid(), gamedailyId);
            if(fansInfoEntity!=null){
                //执行更新操作
                infoEntity.setId(fansInfoEntity.getId());
            }
            wechatFansInfoRepo.save(infoEntity);
        });
    }

    /**
     * 根据openid跟公众号id查询粉丝信息
     * @param openid
     * @param gamedailyId
     * @return
     */
    public WechatFansInfoEntity findByOpenidAndGamedailyId(String openid,long gamedailyId){
        return wechatFansInfoRepo.findByOpenidAndGamedailyId(openid, gamedailyId);
    }

    /**
     * 判断该粉丝是否已经保存过
     * @param openid
     * @param gamedailyId
     * @return
     */
    private boolean fansExitInCurrentGamedaily(String openid,long gamedailyId){
        WechatFansInfoEntity fansInfoEntity = wechatFansInfoRepo.findByOpenidAndGamedailyId(openid, gamedailyId);
        return fansInfoEntity!=null;
    }

    /**
     * 获取微信粉丝列表
     *
     * @param page  page
     * @param limit limit
     * @return Page
     */
    public Page<WechatFansInfoEntity> getFansListByPage(int page, int limit) {
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        Pageable pageable = new PageRequest(page, limit, sort);
        return wechatFansInfoRepo.findAll(pageable);
    }

    public WechatMessageEntity saveTextMessage(WechatTextMessage wechatTextMessage,long gamedailyId){
        WechatMessageEntity wechatMessageEntity = WechatTextMessage.convertFor(wechatTextMessage);
        wechatMessageEntity.setGamedailyId(gamedailyId);
        return wechatMessageRepo.save(wechatMessageEntity);
    }

    /**
     * 判断这条消息是否已经接受过
     * @param msgId       微信消息id（注意不是数据库id）
     * @param gamedailyId 公众号id
     * @return
     */
    public boolean messageExit(long msgId,long gamedailyId){
       return  wechatMessageRepo.findByMsgIdAndGamedailyId(msgId,gamedailyId) != null;
    }
}




















