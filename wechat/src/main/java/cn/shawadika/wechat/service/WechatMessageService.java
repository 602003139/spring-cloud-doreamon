package cn.shawadika.wechat.service;

import cn.shawadika.common.repo.wechat.WechatMessageEntity;
import cn.shawadika.wechat.repo.WechatMessageRepo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;


@Service
public class WechatMessageService {

    @Autowired
    private WechatMessageRepo wechatMessageRepo;

    public Page<WechatMessageEntity> getMessagesByPage(int page, int limit, long gamedailyId,String fromUserOpenId){
        Sort sort = new Sort(Sort.Direction.DESC, "createTime");
        Pageable pageable = new PageRequest(page, limit,sort);
        List<Predicate> list = new ArrayList<>();
        Specification<WechatMessageEntity> specification = (root, query, cb)->{
            list.add(cb.equal(root.get("gamedailyId").as(Long.class),gamedailyId));
            if(StringUtils.isNotEmpty(fromUserOpenId)){
                list.add(cb.equal(root.get("fromUserName").as(String.class),fromUserOpenId));
            }
            return cb.and(list.toArray(new Predicate[list.size()]));
        };
        return wechatMessageRepo.findAll(specification,pageable);
    }
}
