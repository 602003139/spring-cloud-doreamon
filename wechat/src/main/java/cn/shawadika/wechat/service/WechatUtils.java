package cn.shawadika.wechat.service;

import cn.shawadika.common.dto.WechatBatchOpenidDTO;
import cn.shawadika.common.dto.WechatFansInfoDTO;
import cn.shawadika.common.dto.WechatFansInfoListDTO;
import cn.shawadika.common.exceptions.ExceptionCode;
import cn.shawadika.common.exceptions.WechatMemberException;
import cn.shawadika.common.repo.wechat.GamedailyEntity;
import cn.shawadika.common.util.HttpRequest;
import cn.shawadika.wechat.config.WechatConfig;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class WechatUtils {

    @Autowired
    private WechatService wechatService;

    private String getAccessToken() throws WechatMemberException {
        //TODO 目前只有一家测试公众号，所以id先写死 1L
        GamedailyEntity gamedaily = wechatService.findGamedailyById(1L);

        String requestUrl = WechatConfig.ACCESS_TOKEN_URL.
                replaceAll("APPID", gamedaily.getAppid()).
                replaceAll("APPSECRET", gamedaily.getAppsecret());
        String accessToken;
        String result = HttpRequest.sendGet(requestUrl, "");
        if (!StringUtils.isNotEmpty(result)) {
            throw new WechatMemberException(ExceptionCode.WECHAT_ERROR,"请求结果为空");
        }
        JSONObject jsonObject = JSON.parseObject(result);
        String errcode = jsonObject.getString("errcode");
        if (StringUtils.isNotEmpty(errcode)) {
            throw new WechatMemberException(errcode,"获取token失败:"+jsonObject.getString("errmsg"));
        }
        accessToken = jsonObject.getString("access_token");
        return accessToken;
    }

    /**
     * 获取用户列表
     * 一次拉取调用最多拉取10000个关注者的OpenID
     * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140840
     * @param nextOpenId 下一个openid（用来类似于分页的效果） 可以为空，为空则从第一个开始获取
     */
    public WechatBatchOpenidDTO getOpenidBatch(String nextOpenId) throws WechatMemberException {
        String requestUrl = WechatConfig.GET_USER_OPENID_LIST.replaceAll("ACCESS_TOKEN", this.getAccessToken());
        String result;
        if (StringUtils.isNotEmpty(nextOpenId)) {
            result = HttpRequest.sendGet(requestUrl, "&next_openid=" + nextOpenId);
        } else {
            result = HttpRequest.sendGet(requestUrl, "");
        }
        if (!StringUtils.isNotEmpty(result)) {
            throw new WechatMemberException(ExceptionCode.WECHAT_ERROR,"请求结果为空");
        }
        JSONObject jsonObject = JSON.parseObject(result);
        String errcode = jsonObject.getString("errcode");
        if (StringUtils.isNotEmpty(errcode)) {
            throw new WechatMemberException(errcode,"获取token失败:"+jsonObject.getString("errmsg"));
        }
        return JSON.parseObject(result, WechatBatchOpenidDTO.class);
    }

    /**
     * 获取用户基本信息(UnionID机制)
     * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140839
     *
     * @param openid openid
     * @return WechatMemberInfoDTO
     */
    public WechatFansInfoDTO getWechatFansInfoByOpenid(String openid) {
        String requestUrl = WechatConfig.GET_USER_INFO.
                replaceAll("ACCESS_TOKEN", this.getAccessToken()).
                replaceAll("OPENID", openid);
        String result = HttpRequest.sendGet(requestUrl, "");
        if (!StringUtils.isNotEmpty(result)) {
            throw new WechatMemberException(ExceptionCode.WECHAT_ERROR,"请求结果为空");
        }
        JSONObject jsonObject = JSON.parseObject(result);
        String errcode = jsonObject.getString("errcode");
        if (StringUtils.isNotEmpty(errcode)) {
            throw new WechatMemberException(errcode,"获取用户信息失败:"+jsonObject.getString("errmsg"));
        }
        return JSON.parseObject(result, WechatFansInfoDTO.class);
    }

    /**
     * 批量获取用户基本信息(最多支持一次拉取100条。)
     * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140839
     *
     * @param openidList openidList
     * @return List<WechatMemberInfoDTO>
     */
    public WechatFansInfoListDTO getWechatFansInfoByOpenidList(List<String> openidList) throws WechatMemberException {
        if (openidList == null || openidList.size()==0 || openidList.size()>WechatConfig.MAX_FANS_INFO_CAN_PULL_NUM) {
            throw new WechatMemberException(ExceptionCode.WECHAT_ERROR
                    ,"openidList不能为空,且每次最多可以拉取"+WechatConfig.MAX_FANS_INFO_CAN_PULL_NUM+"个用户信息");
        }
        String requestUrl = WechatConfig.GET_USERS_INFO.replaceAll("ACCESS_TOKEN", this.getAccessToken());
        List<JSONObject> jsonObjectList= new ArrayList<JSONObject>(){
            {
                for (String openid : openidList) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("openid",openid);
                    jsonObject.put("lang","zh_CN");
                    add(jsonObject);
                }
            }
        };
        Map<String,Object> outerMap= new HashMap<String,Object>(){
            {
                put("user_list",jsonObjectList);
            }
        };
        String jsonString = JSON.toJSONString(outerMap);
        String result = HttpRequest.sendPost(requestUrl, jsonString);
        if(!StringUtils.isNotEmpty(result)){
            throw new WechatMemberException(ExceptionCode.WECHAT_ERROR,"请求结果为空");
        }
        JSONObject jsonObject = JSON.parseObject(result);
        String errcode = jsonObject.getString("errcode");
        if (StringUtils.isNotEmpty(errcode)) {
            throw new WechatMemberException(errcode,"批量获取用户信息失败:"+jsonObject.getString("errmsg"));
        }
        return JSON.parseObject(result, WechatFansInfoListDTO.class);
    }
}
