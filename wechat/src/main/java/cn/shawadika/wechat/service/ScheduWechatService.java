package cn.shawadika.wechat.service;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.bean.message.WechatTextMessage;
import cn.shawadika.wechat.fallback.ScheduWechatServiceFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "service-web",fallback = ScheduWechatServiceFallback.class)
public interface ScheduWechatService {

    @PostMapping("/websocket/gamedailyTextMessage")
    BaseRestful sendWechatTextMessageToWeb(@RequestBody WechatTextMessage wechatTextMessage);
}
