package cn.shawadika.wechat.repo;

import cn.shawadika.common.repo.wechat.WechatFansInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WechatFansInfoRepo extends JpaRepository<WechatFansInfoEntity,Long> {

    WechatFansInfoEntity findByOpenidAndGamedailyId(String openid,long gamedailyId);

}
