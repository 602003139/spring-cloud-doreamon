package cn.shawadika.wechat.repo;

import cn.shawadika.common.repo.wechat.WechatTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WechatTokenRepo extends JpaRepository<WechatTokenEntity,Long> {
}
