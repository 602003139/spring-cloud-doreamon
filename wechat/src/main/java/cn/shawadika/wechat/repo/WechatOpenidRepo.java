package cn.shawadika.wechat.repo;

import cn.shawadika.common.repo.wechat.WechatOpenidEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WechatOpenidRepo extends JpaRepository<WechatOpenidEntity,Long> {
}
