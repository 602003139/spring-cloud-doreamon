package cn.shawadika.wechat.repo;

import cn.shawadika.common.repo.wechat.GamedailyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GamedailyRepo extends JpaRepository<GamedailyEntity,Long> {
}
