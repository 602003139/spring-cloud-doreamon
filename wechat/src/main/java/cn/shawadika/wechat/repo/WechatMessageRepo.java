package cn.shawadika.wechat.repo;

import cn.shawadika.common.repo.wechat.WechatMessageEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WechatMessageRepo extends JpaRepository<WechatMessageEntity,Long> {

    WechatMessageEntity findByMsgIdAndGamedailyId(long msgId,long gamedailyId);

    Page<WechatMessageEntity> findAllByGamedailyId(Pageable pageable,long gamedailyId);

    Page<WechatMessageEntity> findAll(Specification<WechatMessageEntity> specification, Pageable pageable);
}
