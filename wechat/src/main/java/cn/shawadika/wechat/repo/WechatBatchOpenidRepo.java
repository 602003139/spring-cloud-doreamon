package cn.shawadika.wechat.repo;

import cn.shawadika.common.repo.wechat.WechatBatchOpenidEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WechatBatchOpenidRepo extends JpaRepository<WechatBatchOpenidEntity,Long>{

}
