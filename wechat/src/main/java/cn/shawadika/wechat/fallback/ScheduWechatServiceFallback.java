package cn.shawadika.wechat.fallback;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.bean.message.WechatTextMessage;
import cn.shawadika.common.dto.WechatInputMessageDTO;
import cn.shawadika.wechat.service.ScheduWechatService;
import org.springframework.stereotype.Component;

@Component
public class ScheduWechatServiceFallback implements ScheduWechatService{

    @Override
    public BaseRestful sendWechatTextMessageToWeb(WechatTextMessage wechatTextMessage) {
        return BaseRestful.error("服务器调用异常");
    }
}
