package cn.shawadika.wechat.controller;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.dto.PageDTO;
import cn.shawadika.common.dto.WechatBatchOpenidDTO;
import cn.shawadika.common.dto.WechatFansInfoDTO;
import cn.shawadika.common.dto.WechatFansInfoListDTO;
import cn.shawadika.common.repo.wechat.WechatBatchOpenidEntity;
import cn.shawadika.common.repo.wechat.WechatFansInfoEntity;
import cn.shawadika.common.repo.wechat.WechatOpenidEntity;
import cn.shawadika.wechat.config.WechatConfig;
import cn.shawadika.wechat.service.WechatService;
import cn.shawadika.wechat.service.WechatUtils;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController("wechat_controller")
@RequestMapping("/wechat")
public class WechatController {

    @Autowired
    private WechatService wechatService;
    @Autowired
    private WechatUtils wechatUtils;

    /**
     * 批量获取所有关注者的openid
     * 一次最多拉取10000个
     * 每次调用这个方法，都会去重新拉取openid，在查询微信粉丝详细信息时候，直接查询最新批次的openid对应的信息
     */
    @PostMapping("/build_openid")
    public void buildOpenidAndBatch(@RequestParam(value = "gamedailyId") long gamedailyId) {
        //从微信端获取所有关注者的openid
        WechatBatchOpenidDTO batch = wechatUtils.getOpenidBatch("");
        if (batch != null) {
            if (batch.getTotal() < WechatConfig.MAX_OPENID_CAN_PULL_NUM) {
                wechatService.buildWechatOpenidAndBatch(batch,gamedailyId);
            } else {
                //TODO 当微信关注数量超过10000时需要做一些处理(类似于分页查询)
            }
        }
    }

    /**
     * 粉丝信息的获取是分批次获取的
     * 首先先获取一个批次的openid，通过该批次的openid来批量获取粉丝信息
     *
     * @return
     */
    @PostMapping("/build_fans")
    public BaseRestful buildFans(@RequestParam(value = "gamedailyId") long gamedailyId) {
        //先获取最新批次的openid
        WechatBatchOpenidEntity lastedBatch = wechatService.getLastedBatchOpenid();
        if (lastedBatch != null) {
            List<WechatOpenidEntity> wechatOpenidEntity = lastedBatch.getWechatOpenidEntityList();
            List<String> openidList = new ArrayList<String>() {
                {
                    wechatOpenidEntity.forEach(s -> add(s.getOpenid()));
                }
            };
            int maxSize = WechatConfig.MAX_FANS_INFO_CAN_PULL_NUM;
            if (openidList.size() > 0 && openidList.size() <= maxSize) {
                this.buildFansInfoByOpenidList(openidList,gamedailyId);
            } else if (openidList.size() > maxSize) {
                //需要拉取的次数（每次最多100个）
                double count = Math.ceil((double) openidList.size() / maxSize);
                List<String> subList ;
                int toIndex;
                for (int i = 0; i < count; i++) {
                    toIndex =(i + 1) * maxSize;
                    if(toIndex>openidList.size()){
                        toIndex=openidList.size();
                    }
                    subList = openidList.subList(i * maxSize, toIndex);
                    this.buildFansInfoByOpenidList(subList,gamedailyId);
                }
            } else {
                return BaseRestful.error("该批次的openid为空");
            }
            return BaseRestful.success();
        } else {

        }
        return BaseRestful.error("没有可以获取的批次");
    }

    private void buildFansInfoByOpenidList(List<String> openidList,long gamedailyId){
        WechatFansInfoListDTO wechatFansInfoByOpenidList = wechatUtils.getWechatFansInfoByOpenidList(openidList);
        List<WechatFansInfoDTO> userInfoList = wechatFansInfoByOpenidList.getUser_info_list();
        List<WechatFansInfoEntity> infoEntityList = WechatFansInfoDTO.convertToEntity(userInfoList);
        wechatService.batchSaveWechatFansInfo(infoEntityList, gamedailyId);
    }

    @GetMapping("/fans")
    public PageDTO<WechatFansInfoEntity> getWechatFansList(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "limit", defaultValue = "10") int limit) {
        page = page > 0 ? page - 1 : 0;
        Page<WechatFansInfoEntity> fansInfoEntities = wechatService.getFansListByPage(page, limit);
        PageDTO<WechatFansInfoEntity> pageDTO = new PageDTO<>();
        return pageDTO.covertToPageDTO(fansInfoEntities);
    }

    @GetMapping("/fans/{gamedailyId}/{openid}")
    public BaseRestful getWechatFansInfo(@PathVariable("openid") String openid, @PathVariable("gamedailyId") long gamedailyId){
        WechatFansInfoEntity wechatFansInfo = wechatService.findByOpenidAndGamedailyId(openid, gamedailyId);
        return BaseRestful.success((Object) JSON.toJSONString(wechatFansInfo));
    }


}



















