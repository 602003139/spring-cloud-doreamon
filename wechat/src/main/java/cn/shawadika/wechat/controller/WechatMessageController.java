package cn.shawadika.wechat.controller;

import cn.shawadika.common.dto.PageDTO;
import cn.shawadika.common.repo.wechat.WechatMessageEntity;
import cn.shawadika.wechat.service.WechatMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/wechat/message")
public class WechatMessageController {

    @Autowired
    private WechatMessageService wechatMessageService;

    @GetMapping("/all/{gamedailyId}")
    public PageDTO<WechatMessageEntity> getMessages(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "limit", defaultValue = "10") int limit,
            @PathVariable("gamedailyId") long gamedailyId,
            @RequestParam(value = "openId",defaultValue = "") String openId) {
        page = page > 0 ? page - 1 : page;
        Page<WechatMessageEntity> messagesByPage = wechatMessageService.getMessagesByPage(page, limit, gamedailyId,openId);
        PageDTO<WechatMessageEntity> pageDTO = new PageDTO<>();
        return pageDTO.covertToPageDTO(messagesByPage);
    }
}
