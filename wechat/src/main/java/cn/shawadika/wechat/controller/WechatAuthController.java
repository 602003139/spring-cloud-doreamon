package cn.shawadika.wechat.controller;

import cn.shawadika.common.dto.WechatInputMessageDTO;
import cn.shawadika.common.bean.message.WechatTextMessage;
import cn.shawadika.common.constants.WechatMessageTypeConstant;
import cn.shawadika.common.repo.wechat.GamedailyEntity;
import cn.shawadika.common.util.SerializeXmlUtil;
import cn.shawadika.common.util.StreamUtil;
import cn.shawadika.wechat.service.ScheduWechatService;
import cn.shawadika.wechat.service.WechatService;
import com.thoughtworks.xstream.XStream;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Arrays;

@RestController
@RequestMapping("/wechat")
public class WechatAuthController {

    @Autowired
    private WechatService wechatService;
    @Autowired
    private ScheduWechatService scheduWechatService;

    /**
     * 微信服务器将发送GET请求到填写的服务器地址URL上
     * 原样返回echostr参数内容，则接入生效，成为开发者成功
     *
     * @param signature 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
     * @param timestamp 时间戳
     * @param nonce     随机数
     * @param echostr   随机字符串
     * @return
     */
    @RequestMapping("/auth")
    public String auth(String signature, String timestamp, String nonce, String echostr, HttpServletRequest request) throws IOException {

        if (StringUtils.isNotEmpty(signature) && StringUtils.isNotEmpty(timestamp) && StringUtils.isNotEmpty(nonce)) {
            //TODO 目前只有一个公众号，所以先写死，后面这个要想办法
            boolean b = this.checkSignature(signature, timestamp, nonce, 1L);
            if (b) {
                WechatInputMessageDTO wechatInputMessageDTO = this.acceptWechatMessage(request);
                if (wechatInputMessageDTO != null) {
                    //TODO feign 在服务器长时间未响应会进行重试，导致消息重复发送，这个要做处理
                    switch (wechatInputMessageDTO.getMsgType()) {
                        case WechatMessageTypeConstant.TEXT_MESSAGE:
                            WechatTextMessage wechatTextMessage = WechatTextMessage.build(wechatInputMessageDTO);
                            //判断消息是否重复发送（微信会有重试机制）
                            boolean exit = wechatService.messageExit(wechatTextMessage.getMsgId(), 1L);
                            if(!exit){
                                //保存消息到数据库
                                //TODO 目前只有一个公众号，所以先写死，后面这个要想办法
                                wechatService.saveTextMessage(wechatTextMessage, 1L);
                                //将消息推到web端
                                scheduWechatService.sendWechatTextMessageToWeb(wechatTextMessage);
                            }
                            break;
                        case WechatMessageTypeConstant.IMG_MESSAGE:
                            break;

                        case WechatMessageTypeConstant.LINK_MESSAGE:
                            break;

                        case WechatMessageTypeConstant.LOCATION_MESSAGE:
                            break;

                        case WechatMessageTypeConstant.SHORTVIDEO_MESSAGE:
                            break;

                        case WechatMessageTypeConstant.VIDEO_MESSAGE:
                            break;

                        case WechatMessageTypeConstant.VOICE_MESSAGE:
                            break;
                        default:
                            System.out.println("不支持的微信消息类型：" + wechatInputMessageDTO.getMsgType());
                    }
                }
                return echostr;
            }
        }
        return "error";
    }

    /**
     * 处理微信端推送的消息
     * http://blog.csdn.net/morning99/article/details/43865471
     */
    private WechatInputMessageDTO acceptWechatMessage(HttpServletRequest request) throws IOException {
        ServletInputStream in = request.getInputStream();
        // 将POST流转换为XStream对象
        XStream xs = SerializeXmlUtil.createXstream();
        //对指定的类使用Annotations 进行序列化
        xs.processAnnotations(WechatInputMessageDTO.class);
        // 将指定节点下的xml节点数据映射为对象
        xs.alias("xml", WechatInputMessageDTO.class);
        // 将流转换为字符串
        String xmlMsg = StreamUtil.convertToStr(in);
        if (StringUtils.isNotEmpty(xmlMsg)) {
            return (WechatInputMessageDTO) xs.fromXML(xmlMsg);
        }
        return null;
    }

    private boolean checkSignature(String signature, String timestamp, String nonce, long gamedailyId) {
        boolean result = false;
        GamedailyEntity gamedaily = wechatService.findGamedailyById(gamedailyId);
        // 1.将token、timestamp、nonce三个参数进行字典序排序
        String[] array = new String[]{gamedaily.getToken(), timestamp, nonce};
        Arrays.sort(array);
        // 将三个参数字符拼接成一个字符串
        String str = array[0].concat(array[1]).concat(array[2]);
        String sha1Str;
        try {
            // 对拼接后的字符串进行sha1加密
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] digest = md.digest(str.getBytes());
            sha1Str = byte2str(digest);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        if (sha1Str.equals(signature)) {
            result = true;
        }
        return result;
    }

    /**
     * 将字节数组转换成字符串
     */
    private static String byte2str(byte[] array) {
        StringBuffer hexstr = new StringBuffer();
        String shaHex;
        for (int i = 0; i < array.length; i++) {
            shaHex = Integer.toHexString(array[i] & 0xFF);
            if (shaHex.length() < 2) {
                hexstr.append(0);
            }
            hexstr.append(shaHex);
        }
        return hexstr.toString();
    }
}








