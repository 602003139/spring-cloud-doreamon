package cn.shawadika.wechat.config;

public interface WechatConfig {

    /*String APPID="wx41e3f03818ed49f6";
    String APPSECRET="9ee588d5cdd2dccf71b9b8defd3b27fc";
    String TOKEN="7T2OABM9X68RNN5IG99IJ76HLFX6SD31";*/

    /**获取用户列表:一次拉取调用最多拉取10000个关注者的OpenID*/
    Integer MAX_OPENID_CAN_PULL_NUM=10000;
    /**在批量获取用户信息的时候，每次最多可以拉取的个数*/
    Integer MAX_FANS_INFO_CAN_PULL_NUM=100;

    /**获取token*/
    String ACCESS_TOKEN_URL="https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";
    /**获取用户openId列表*/
    String GET_USER_OPENID_LIST ="https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN";
    /**获取用户信息*/
    String GET_USER_INFO="https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN ";
    /**批量获取用户基本信息*/
    String GET_USERS_INFO="https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=ACCESS_TOKEN";
}
