# 基于那个镜像
FROM frolvlad/alpine-oraclejdk8 

# 安装软件用
# RUN 

# 镜像创建者
MAINTAINER fzlinjianhui@163.com

# container启动时执行的命令，但是一个Dockerfile中只能有一条CMD命令，多条则只执行最后一条CMD.
# CMD [ "executable" ]

# 可以将本地文件夹或者其他container的文件夹挂载到container中。
VOLUME D:/volumes/resource

# 将文件<src>拷贝到container的文件系统对应的路径<dest>
# 所有拷贝到container中的文件和文件夹权限为0755,uid和gid为0
# 如果要ADD本地文件，则本地文件必须在 docker build <PATH>，指定的<PATH>目录下
# 如果要ADD远程文件，则远程文件必须在 docker build <PATH>，指定的<PATH>目录下。比如:
# docker build github.com/creack/docker-firefox
ADD ./web-0.0.1.jar app.jar

# container启动时执行的命令，但是一个Dockerfile中只能有一条ENTRYPOINT命令，如果多条，则只执行最后一条
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]

# container内部服务开启的端口。主机上要用还得在启动container时，做host-container的端口映射：
# docker run -d -p 127.0.0.1:33301:22 centos6-ssh
EXPOSE 8765

# 用来设置环境变量
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# ONBUILD 指定的命令在构建镜像时并不执行，而是在它的子镜像中执行
# ONBUILD