CREATE TABLE members
(
  id        BIGINT AUTO_INCREMENT  PRIMARY KEY,
  nick_name      VARCHAR(255)    NOT NULL COMMENT '昵称',
  sex       INT DEFAULT 0 NOT NULL COMMENT '1男,2女',
  account VARCHAR(255)    NOT NULL COMMENT '登录名',
  head_img_url VARCHAR(255)    NOT NULL COMMENT '头像地址',
  password  VARCHAR(32)    NOT NULL COMMENT '密码',
  encrypt   VARCHAR(6)    NOT NULL COMMENT '加密字符串'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COMMENT '用户表';

CREATE TABLE wechat_openid_batch
(
  id            BIGINT AUTO_INCREMENT  PRIMARY KEY,
  create_time   DATETIME  NOT NULL COMMENT '获取时间',
  next_openid   VARCHAR(255) DEFAULT NULL COMMENT '下一次获取的起始openid',
  total         BIGINT COMMENT '该批次总条数',
  count         INT NOT NULL COMMENT '拉取的OPENID个数',
  gamedaily_id     BIGINT NOT NULL COMMENT '微信公众号表id'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '获取微信openid批次';

CREATE TABLE wechat_openid
(
  id                       BIGINT AUTO_INCREMENT  PRIMARY KEY,
  openid                   VARCHAR(255)  NOT NULL COMMENT 'openid',
  openid_batch_id   INT DEFAULT 0 NOT NULL COMMENT '批次id'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '微信openid';

CREATE TABLE wechat_token
(
  id            BIGINT AUTO_INCREMENT  PRIMARY KEY,
  token         VARCHAR(255)  NOT NULL COMMENT 'token',
  create_time   DATETIME  COMMENT '创建时间',
  overdue_time  DATETIME COMMENT '过期时间',
  valid         TINYINT DEFAULT 0 COMMENT '0有效，1无效'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '微信token';

CREATE TABLE wechat_fans_info
(
  id            BIGINT AUTO_INCREMENT  PRIMARY KEY,
  subscribe     TINYINT  NOT NULL COMMENT '是否订阅该公众号标识',
  openid        VARCHAR(255)  COMMENT 'OPENID',
  nickname      VARCHAR(255) COMMENT '昵称',
  sex           TINYINT DEFAULT 1 COMMENT '1男 2女',
  city          VARCHAR(255) DEFAULT '' COMMENT '城市',
  country       VARCHAR(255) DEFAULT '' COMMENT '国家',
  province      VARCHAR(255) DEFAULT '' COMMENT '省份',
  language      VARCHAR(255) DEFAULT 'zh_CN' COMMENT '语言',
  headimgurl    VARCHAR(255) DEFAULT '' COMMENT '头像',
  subscribe_time  VARCHAR(255)  COMMENT '关注时间',
  unionid       VARCHAR(255)  COMMENT '只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段',
  groupid       INT  COMMENT '所在分组id',
  remark        VARCHAR(255)  COMMENT '备注',
  tagid_list    VARCHAR(255) DEFAULT 0 COMMENT '标签id',
  gamedaily_id  BIGINT NOT NULL COMMENT '微信公众号id'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COMMENT '微信用户信息';

CREATE TABLE wechat_gamedaily
(
  id           BIGINT AUTO_INCREMENT  PRIMARY KEY,
  appid        VARCHAR(32) NOT NULL  COMMENT 'appid',
  appsecret    VARCHAR(32) NOT NULL COMMENT 'appsecret',
  token        VARCHAR(32) NOT NULL COMMENT 'token'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '微信公众号信息';
CREATE TABLE wechat_message
(
  id              BIGINT AUTO_INCREMENT  PRIMARY KEY,
  gamedaily_id    BIGINT COMMENT '公众号id' ,
  msg_id      BIGINT NOT NULL COMMENT '消息id',
  to_user_name    VARCHAR(255) NOT NULL COMMENT '开发者微信号',
  from_user_name  VARCHAR(255) NOT NULL COMMENT '发送方帐号（一个OpenID）',
  create_time     BIGINT COMMENT '消息创建时间',
  msg_type        VARCHAR(25) COMMENT '消息类型',
  content         TEXT COMMENT '消息内容',
  pic_url         VARCHAR(255)  COMMENT '图片链接',
  location_x      VARCHAR(255)  COMMENT '地理位置维度',
  location_y      VARCHAR(255)  COMMENT '地理位置经度',
  scale           BIGINT COMMENT '地图缩放大小',
  label           VARCHAR(255)  COMMENT '地理位置信息',
  title           VARCHAR(255)  COMMENT '消息标题',
  description     VARCHAR(255)  COMMENT '消息描述',
  url             VARCHAR(255)  COMMENT '消息链接',
  media_id        BIGINT COMMENT '语音消息媒体id，可以调用多媒体文件下载接口拉取该媒体',
  format          VARCHAR(255)  COMMENT '语音格式：amr',
  recognition     VARCHAR(255)  COMMENT '语音识别结果，UTF8编码',
  event           VARCHAR(255)  COMMENT '事件类型',
  event_key       VARCHAR(255)  COMMENT '事件KEY值，qrscene_为前缀，后面为二维码的参数值',
  ticket          VARCHAR(255)  COMMENT '二维码的ticket，可用来换取二维码图片'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COMMENT '微信公众号消息';

INSERT INTO wechat_gamedaily(appid,appsecret,token) VALUES ('wx41e3f03818ed49f6','9ee588d5cdd2dccf71b9b8defd3b27fc','7T2OABM9X68RNN5IG99IJ76HLFX6SD31');
INSERT INTO members(nick_name,sex,account,head_img_url,password,encrypt) VALUES ('大灰灰',1,'602003139@qq.com','http://wx.qlogo.cn/mmopen/Q3auHgzwzM5eMSqeGEfGpfialPSicVuy5Pux6flZPNHVicB95umlunC3jFLKdl53nnhJa9PXMA7BqOjLZdKYdC7NQ/0','6B1395EBF81D6EC24F18A119D96A97C3','Ki6SQJ') ;