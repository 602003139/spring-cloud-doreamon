package cn.shawadika.member.repo;

import cn.shawadika.common.repo.member.MemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepo extends JpaRepository<MemberEntity,Long> {

    MemberEntity findMemberEntityByAccount(String account);
}
