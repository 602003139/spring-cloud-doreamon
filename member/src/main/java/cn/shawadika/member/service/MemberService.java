package cn.shawadika.member.service;

import cn.shawadika.common.repo.member.MemberEntity;
import cn.shawadika.common.util.Md5;
import cn.shawadika.common.util.SerialNumber;
import cn.shawadika.member.repo.MemberRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class MemberService {

    private static final int MEMBER_ENCRYPT_LENGTH=6;
    private static final int MEMBER_SEX_DEFAULT=1;
    private static final String MEMBER_NICKNAME_DEFAULT="大灰灰";
    private static final String MEMBER_HEAD_IMG_DEFAULT="http://wx.qlogo.cn/mmopen/Q3auHgzwzM5eMSqeGEfGpfialPSicVuy5Pux6flZPNHVicB95umlunC3jFLKdl53nnhJa9PXMA7BqOjLZdKYdC7NQ/0";

    @Autowired
    private MemberRepo memberRepo;

    public MemberEntity findByAccount(String account){
        return memberRepo.findMemberEntityByAccount(account);
    }

    public String makePassword(String password,String encrypt){
        return Md5.getMD5(password + encrypt);
    }

    public String makeEncrypt(){
        return SerialNumber.randomString(MEMBER_ENCRYPT_LENGTH);
    }

    public MemberEntity build(String account,String password,String encrypt){
        return MemberEntity.builder()
                .nickName(MEMBER_NICKNAME_DEFAULT)
                .account(account)
                .password(password)
                .encrypt(encrypt)
                .sex(MEMBER_SEX_DEFAULT)
                .headImgUrl(MEMBER_HEAD_IMG_DEFAULT)
                .build();
    }

    public MemberEntity save(MemberEntity memberEntity){
        MemberEntity entity = this.findByAccount(memberEntity.getAccount());
        if(entity!=null){
            return null;
        }
        return memberRepo.save(memberEntity);
    }

    public Page<MemberEntity> findByPage(int page, int limit){
        Sort sort = new Sort(Sort.Direction.ASC, "id");
        Pageable pageable = new PageRequest(page, limit,sort);
        return memberRepo.findAll(pageable);
    }
}
