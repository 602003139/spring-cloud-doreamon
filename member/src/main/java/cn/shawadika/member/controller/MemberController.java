package cn.shawadika.member.controller;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.dto.MemberDTO;
import cn.shawadika.common.dto.PageDTO;
import cn.shawadika.common.repo.member.MemberEntity;
import cn.shawadika.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import static cn.shawadika.common.Restful.BaseRestful.MEMBER_ERROR_CODE;

@RestController("member_controller")
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @PostMapping("/build")
    public BaseRestful build(@RequestParam(value = "account") String account,
                             @RequestParam(value = "password") String password) {
        String encrypt = memberService.makeEncrypt();
        String passwordEncrypt = memberService.makePassword(password, encrypt);
        MemberEntity memberEntity = memberService.build(account, passwordEncrypt, encrypt);
        MemberEntity entity = memberService.save(memberEntity);
        if (entity == null) {
            return BaseRestful.error(MEMBER_ERROR_CODE, "该账号已存在");
        }
        return BaseRestful.success(entity);
    }

    @GetMapping("/find")
    public MemberEntity find(@RequestParam(value = "account") String account,
                             @RequestParam(value = "password") String password) {
        MemberEntity memberEntity = memberService.findByAccount(account);
        if (memberEntity != null) {
            String passwordEncrypt = memberService.makePassword(password, memberEntity.getEncrypt());
            if (passwordEncrypt.equals(memberEntity.getPassword())) {
                return memberEntity;
            }
        }
        return null;
    }

    @GetMapping("/list")
    public PageDTO<MemberDTO> buildMemberList(
            @RequestParam(name = "page", defaultValue = "0") int page,
            @RequestParam(name = "limit", defaultValue = "10") int limit) {
        page = page > 0 ? page - 1 : 0;
        Page<MemberEntity> memberEntities = memberService.findByPage(page, limit);
        Page<MemberDTO> memberDTOS = memberEntities.map(MemberDTO::coverToMemberDTO);
        PageDTO<MemberDTO> pageDTO = new PageDTO<>();
        return pageDTO.covertToPageDTO(memberDTOS);
    }
}















