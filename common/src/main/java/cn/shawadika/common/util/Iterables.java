package cn.shawadika.common.util;

import java.util.Objects;
import java.util.function.BiConsumer;

/**
 * 因为java8没有提供一个带有索引的iterables，所以手动实现一个
 * https://segmentfault.com/a/1190000007881498
 */
public class Iterables {
    public static <E> void forEach(
            Iterable<? extends E> elements, BiConsumer<Integer, ? super E> action) {
        Objects.requireNonNull(elements);
        Objects.requireNonNull(action);

        int index = 0;
        for (E element : elements) {
            action.accept(index++, element);
        }
    }
}
