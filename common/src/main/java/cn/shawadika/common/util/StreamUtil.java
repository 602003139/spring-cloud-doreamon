package cn.shawadika.common.util;

import java.io.IOException;
import java.io.InputStream;

public class StreamUtil {

    public static String convertToStr(InputStream in) throws IOException {
        StringBuilder builder = new StringBuilder();
        byte[] b = new byte[4096];
        for (int n; (n = in.read(b)) != -1;) {
            builder.append(new String(b, 0, n, "UTF-8"));
        }
        return builder.toString();
    }
}
