package cn.shawadika.common.util;

import cn.shawadika.common.dto.ArithmeticDTO;

import java.util.Random;

/***
 * 生成加减运算
 * 用于前端的人类验证
 */
public class ArithmeticUtil {

    public static ArithmeticDTO generateArithmetic() {
        String[] operatorArray = new String[]{"+", "-",""};
        StringBuilder sb = new StringBuilder("");
        int a = randomNum(10);
        int b = randomNum(10);
        String result = "大灰灰";
        String operator = operatorArray[randomNum(3)];
        switch (operator) {
            case "+":
                result = String.valueOf((a + b));
                sb.append(a).append("加").append(b).append("等于多少？");
                break;
            case "-":
                result = String.valueOf((a - b));
                sb.append(a).append("减").append(b).append("等于多少？");
                break;
            default:
                sb.append("这个网站是谁写的？");
        }
        return ArithmeticDTO.builder().question(sb.toString()).result(result).build();
    }

    private static int randomNum(int num) {
        Random random = new Random();
        return random.nextInt(num);
    }
}
