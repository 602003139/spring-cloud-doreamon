package cn.shawadika.common.dto;

import cn.shawadika.common.repo.member.MemberEntity;
import com.google.common.base.Converter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MemberDTO {

    private Long id;

    private String nickName;

    private String account;

    private Integer sex;

    private String headImgUrl;

    public static MemberDTO coverToMemberDTO(MemberEntity memberEntity){
        return new MemberDTOConverter().doBackward(memberEntity);
    }

    public static List<MemberDTO> covertToMemberDTOs(List<MemberEntity> memberEntities){
        List<MemberDTO> list=new ArrayList<>();
        for (MemberEntity memberEntity : memberEntities){
            list.add(coverToMemberDTO(memberEntity));
        }
        return list;
    }

    private static class MemberDTOConverter extends Converter<MemberDTO,MemberEntity> {

        @Override
        protected MemberEntity doForward(MemberDTO memberDTO) {
            MemberEntity memberEntity = new MemberEntity();
            BeanUtils.copyProperties(memberDTO,memberEntity);
            return memberEntity;
        }

        @Override
        protected MemberDTO doBackward(MemberEntity memberEntity) {
            MemberDTO memberDTO = new MemberDTO();
            BeanUtils.copyProperties(memberEntity,memberDTO);
            return memberDTO;
        }
    }

}
