package cn.shawadika.common.dto;

import lombok.Data;

import java.util.List;

@Data
public class WechatOpenidDTO {

    private List<String> openid;
}
