package cn.shawadika.common.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PageDTO<T>{

    protected List<T> data;

    protected Boolean last;

    protected Boolean first;

    protected Integer totalPages;

    protected Long totalElements;

    protected Integer number;

    protected Integer size;

    protected Integer numberOfElements;

    public PageDTO<T> covertToPageDTO(Page<T> page){
        PageDTO<T> pageDTO = new PageDTO<>();
        pageDTO.setData(page.getContent());
        pageDTO.setNumber(page.getNumber());
        pageDTO.setSize(page.getSize());
        pageDTO.setTotalElements(page.getTotalElements());
        pageDTO.setFirst(page.isFirst());
        pageDTO.setLast(page.isLast());
        pageDTO.setTotalPages(page.getTotalPages());
        pageDTO.setNumberOfElements(page.getNumberOfElements());
        return pageDTO;
    }

    public PageDTO<Object> covertToPageDTO(PageDTO<Object> pageDTO,List<Object> list){
        pageDTO.setData(list);
        return pageDTO;
    }
}
