package cn.shawadika.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class WechatBatchOpenidDTO {

    private Integer total;

    private Integer count;

    private String nextOpenid;

    private WechatOpenidDTO data;

}
