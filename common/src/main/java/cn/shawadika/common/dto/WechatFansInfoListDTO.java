package cn.shawadika.common.dto;

import lombok.Data;

import java.util.List;

@Data
public class WechatFansInfoListDTO {

    List<WechatFansInfoDTO> user_info_list;
}
