package cn.shawadika.common.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class ArithmeticDTO {

    private String question;

    private String result;
}
