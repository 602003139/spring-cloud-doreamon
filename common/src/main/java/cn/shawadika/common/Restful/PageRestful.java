package cn.shawadika.common.Restful;

import cn.shawadika.common.dto.PageDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

import java.util.List;


@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class PageRestful extends BaseRestful {

    private List data;

    private Boolean last;

    private Boolean first;

    private Integer totalPages;

    private Long totalElements;

    private Integer number;

    private Integer size;

    private Integer numberOfElements;

    public static PageRestful success(PageDTO pageDTO){
        PageRestful pageRestful = new PageRestful();
        BeanUtils.copyProperties(pageDTO,pageRestful);
        pageRestful.setCode(SUCCESS_CODE);
        pageRestful.setMessage(SUCCESS_MESSAGE);
        return pageRestful;
    }

    public static PageRestful error(String message){
        PageRestful pageRestful = new PageRestful();
        pageRestful.setCode(SYSTEM_ERROR_CODE);
        pageRestful.setMessage(message);
        return pageRestful;
    }
}
