package cn.shawadika.common.Restful;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BaseRestful {

    public static final String SUCCESS_CODE="100";
    public static final String SYSTEM_ERROR_CODE ="400";
    public static final String WECHAT_ERROR_CODE ="401";
    public static final String MEMBER_ERROR_CODE ="402";

    public static final String SUCCESS_MESSAGE="SUCCESS";
    public static final String SYSTEM_ERROR_MESSAGE ="系统错误";
    public static final String WECHAT_ERROR_MESSAGE="与微信服务器通信失败";

    protected String code;

    protected String message;

    protected Object data;

    public static BaseRestful success(){
        return BaseRestful.builder().code(SUCCESS_CODE).message(SUCCESS_MESSAGE).build();
    }

    public static BaseRestful success(String message){
        return BaseRestful.builder().code(SUCCESS_CODE).message(message).build();
    }

    public static BaseRestful success(String message,Object data){
        return BaseRestful.builder().code(SUCCESS_CODE).message(message).data(data).build();
    }

    public static BaseRestful success(Object data){
        return BaseRestful.builder().code(SUCCESS_CODE).message(SUCCESS_MESSAGE).data(data).build();
    }

    public static BaseRestful error(String message){
        return BaseRestful.builder().code(SYSTEM_ERROR_CODE).message(message).build();
    }

    public static BaseRestful error(String code,String message){
        return BaseRestful.builder().code(code).message(message).build();
    }

    public boolean isSuccess(){
        return SUCCESS_CODE.equals(this.code);
    }

}
