package cn.shawadika.common.modules;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class Weather {

    private String addressName;

    private String code;

    private String weatherDesc;

    private String temperature;
}
