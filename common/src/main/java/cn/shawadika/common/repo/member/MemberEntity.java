package cn.shawadika.common.repo.member;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table( name="members")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class MemberEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String nickName;

    private String account;

    private Integer sex;

    private String password;

    private String encrypt;

    private String headImgUrl;
}
