package cn.shawadika.common.repo.spider.fz12345;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

/**
 * 12345诉求列表需要纪录的信息
 */
@Table(name = "spider_appeal")
@Data
@ToString
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Appeal {

    @Id
    @GeneratedValue
    private Long id;

    private String appealNo;//诉求编号

    private String appealType;//诉求类别

    private String type;//诉求类型

    private String appealTitle;//标题

    private String dealResult;//处理情况

    private String instructResult;//批示情况

    private String content;//内容

    private String appealUser;//诉求人

    private String answer;//回复意见

    private String completeTime;//回复时间

    private String dept;//办理部门

    private String source;//来源

    private String clicks;//点击数

    private String appealTime;//诉求时间

    private Date createTime;//系统创建时间
}
