package cn.shawadika.common.repo.wechat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Data
@Table(name="wechat_openid_batch")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class WechatBatchOpenidEntity {

    @Id
    @GeneratedValue
    private Long id;

    private Timestamp createTime;

    private String nextOpenid;

    private Integer total;

    private Integer count;

    private Long gamedailyId;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "openid_batch_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private List<WechatOpenidEntity> wechatOpenidEntityList;
}
