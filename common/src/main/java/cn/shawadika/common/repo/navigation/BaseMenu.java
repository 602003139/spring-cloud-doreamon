package cn.shawadika.common.repo.navigation;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class BaseMenu {

    protected String name;

    protected String url;

    protected String iconClass;

    protected List<ChildMenu> child;
}
