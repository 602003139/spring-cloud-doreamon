package cn.shawadika.common.repo.wechat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Table(name = "wechat_openid")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WechatOpenidEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String openid;

    @Column(name = "openid_batch_id")
    private Long wechatBatchOpenidId;

}
