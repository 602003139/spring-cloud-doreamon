package cn.shawadika.common.repo.wechat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "wechat_gamedaily")
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GamedailyEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String appid;

    private String appsecret;

    private String token;

}
