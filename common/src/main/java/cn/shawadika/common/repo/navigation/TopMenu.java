package cn.shawadika.common.repo.navigation;

import lombok.Data;

@Data
public class TopMenu extends BaseMenu {

    private Integer sort;
}
