package cn.shawadika.common.repo.navigation;

import lombok.Data;

import java.util.List;

@Data
public class Navigation {

    private List<TopMenu> menuList;
}
