package cn.shawadika.common.repo.wechat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "wechat_token")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WechatTokenEntity {

    @Id
    @GeneratedValue
    private Long id;

    private String token;

    private Timestamp createTime;

    /**过期时间*/
    private Timestamp overdueTime;
    /**0有效,1无效*/
    private short valid;
}
