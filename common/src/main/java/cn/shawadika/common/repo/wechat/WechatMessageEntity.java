package cn.shawadika.common.repo.wechat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "wechat_message")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WechatMessageEntity {

    @Id
    @GeneratedValue
    private Long id;

    private Long gamedailyId;

    @Column(name = "msg_id")
    private Long msgId;
    private String toUserName;
    private String fromUserName;
    @Column(name = "create_time")
    private Long createTime;
    private String msgType = "text";
    // 文本消息
    private String content;
    // 图片消息
    private String picUrl;
    // 位置消息
    @Column(name = "location_x")
    private String locationX;
    @Column(name = "location_y")
    private String locationY;
    private Long scale;
    private String label;
    // 链接消息
    private String title;
    private String description;
    private String url;
    // 语音信息
    private Long mediaId;
    private String format;
    private String recognition;
    // 事件
    private String event;
    private String eventKey;
    private String ticket;
}
