package cn.shawadika.common.repo.spider.fz12345;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

/**
 * 抓取纪录
 */
@Entity
@Table(name = "appeal_record")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CatchRecord {

    @Id
    @GeneratedValue
    private Long id;

    private Date startDate;

    private Date endDate;

    private Integer page;

    private String callStatus;//处理情况

    private String zoneId;//地址id

    private short status;//1成功  0失败
}
