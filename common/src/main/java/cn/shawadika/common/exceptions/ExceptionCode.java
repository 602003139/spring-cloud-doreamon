package cn.shawadika.common.exceptions;

public class ExceptionCode {

    public static final String SYSTEM_ERROR = "1000";

    public static final String PARAMS_ERROR = "1001";

    public static final String WECHAT_ERROR = "2000";

    public static final String SAVE_DATA_ERROR = "1100";
}
