package cn.shawadika.common.exceptions;

import lombok.Getter;

@Getter
public class BaseException extends RuntimeException {

    protected String code;

    protected String message;

    public BaseException(String code,String message){
        super(message);
        this.code=code;
        this.message=message;
    }
}
