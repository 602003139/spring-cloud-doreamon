package cn.shawadika.common.exceptions;

import lombok.Getter;

@Getter
public class WechatMemberException extends BaseException {

    public WechatMemberException(String code, String message) {
        super(code, message);
    }
}
