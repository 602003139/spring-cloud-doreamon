package cn.shawadika.common.factory;

import cn.shawadika.common.bean.message.MyMessage;
import cn.shawadika.common.bean.message.SystemMessage;
import cn.shawadika.common.constants.MessageConstant;

public class MessageFactory {

    public static SystemMessage createSystemMessage(String message ,long toUserId){
        SystemMessage systemMessage = new SystemMessage();
        systemMessage.setFromUserId(0L);
        systemMessage.setMessage(message);
        systemMessage.setToUserId(toUserId);
        systemMessage.setMessageType(MessageConstant.SYSTEM_MESSAGE);
        systemMessage.setSendTime(System.currentTimeMillis());
        return systemMessage;
    }

    public static MyMessage createMyMessage(String message,long myUserId){
        MyMessage myMessage = new MyMessage();
        myMessage.setFromUserId(myUserId);
        myMessage.setToUserId(myUserId);
        myMessage.setMessage(message);
        myMessage.setMessageType(MessageConstant.MY_MESSAGE);
        myMessage.setSendTime(System.currentTimeMillis());
        return myMessage;
    }
}
