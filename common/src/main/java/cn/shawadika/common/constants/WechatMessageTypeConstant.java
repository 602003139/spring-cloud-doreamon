package cn.shawadika.common.constants;

/**
 * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140453
 */
public interface WechatMessageTypeConstant {

    String TEXT_MESSAGE = "text";

    String IMG_MESSAGE = "image";

    String VOICE_MESSAGE = "voice";

    String VIDEO_MESSAGE = "video";

    String SHORTVIDEO_MESSAGE = "shortvideo";

    String LOCATION_MESSAGE = "location";

    String LINK_MESSAGE = "link";
}
