package cn.shawadika.common.constants;

public interface MemberConstant {

    String MEMBER_SESSION_KEY = "member_session_key";
    String MEMBER_REGIST_SECURITY_CODE_KEY = "member_regist_security_code_key";
    String MEMBER_REGIST_ARITHMETIC_RESULT_KEY="member_regist_arithmetic_result_key";
    String MEMBER_LOGIN_ARITHMETIC_RESULT_KEY="member_login_arithmetic_result_key";

    int MAX_SECURITY_CODE_LENGTH=6;
}
