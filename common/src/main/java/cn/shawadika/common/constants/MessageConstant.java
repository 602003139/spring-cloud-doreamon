package cn.shawadika.common.constants;

public interface MessageConstant {

    String SYSTEM_MESSAGE="system";

    String WECHAT_MESSAGE="wechat";

    String MY_MESSAGE="myMessage";
}
