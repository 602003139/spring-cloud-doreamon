package cn.shawadika.common.constants;

public interface WebSocketConstant {

    String WEBSOCKET_MEMBER_ID="webSocketMemberId";

    String WEBSOCKET_MEMBER_ACCOUNT="webSocketMemberAccount";
}
