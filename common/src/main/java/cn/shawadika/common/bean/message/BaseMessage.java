package cn.shawadika.common.bean.message;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class BaseMessage{

    protected String message;

    protected Long fromUserId;

    protected Long toUserId;

    protected String messageType;

    protected long sendTime;

}
