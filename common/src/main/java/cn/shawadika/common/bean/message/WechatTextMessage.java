package cn.shawadika.common.bean.message;


import cn.shawadika.common.constants.MessageConstant;
import cn.shawadika.common.dto.WechatInputMessageDTO;
import cn.shawadika.common.repo.wechat.WechatMessageEntity;
import com.google.common.base.Converter;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class WechatTextMessage extends BaseMessage{

    private String fromUserName;

    private String toUserName;

    private String msgType;

    private Long msgId;

    private String wechatFansImgUrl;

    private String wechatFansNickName;

    private WechatTextMessage(){}

    public static WechatTextMessage build(WechatInputMessageDTO inputMessage){
        WechatTextMessage wechatTextMessage = new WechatTextMessage();
        wechatTextMessage.setMessage(inputMessage.getContent());
        wechatTextMessage.setSendTime(inputMessage.getCreateTime());
        wechatTextMessage.setFromUserName(inputMessage.getFromUserName());
        wechatTextMessage.setToUserName(inputMessage.getToUserName());
        wechatTextMessage.setMsgType(inputMessage.getMsgType());
        wechatTextMessage.setMsgId(inputMessage.getMsgId());
        wechatTextMessage.setFromUserId(0L);
        wechatTextMessage.setToUserId(0L);
        wechatTextMessage.setMessageType(MessageConstant.WECHAT_MESSAGE);
        return wechatTextMessage;
    }

    public static WechatMessageEntity convertFor(WechatTextMessage wechatTextMessage){
        WechatTextMessageConvert wechatTextMessageConvert = new WechatTextMessageConvert();
        return wechatTextMessageConvert.reverse().convert(wechatTextMessage);

    }

    public static WechatTextMessage convertBack(WechatMessageEntity wechatMessageEntity){
        WechatTextMessageConvert wechatTextMessageConvert = new WechatTextMessageConvert();
        return wechatTextMessageConvert.doForward(wechatMessageEntity);
    }

    private static class WechatTextMessageConvert extends Converter<WechatMessageEntity,WechatTextMessage>{


        @Override
        protected WechatTextMessage doForward(WechatMessageEntity wechatMessageEntity) {
            WechatTextMessage wechatTextMessage = new WechatTextMessage();
            wechatTextMessage.setFromUserName(wechatMessageEntity.getFromUserName());
            wechatTextMessage.setMsgId(wechatMessageEntity.getMsgId());
            wechatTextMessage.setMsgType(wechatMessageEntity.getMsgType());
            wechatTextMessage.setToUserName(wechatMessageEntity.getToUserName());
            wechatTextMessage.setMessage(wechatMessageEntity.getContent());
            wechatTextMessage.setSendTime(wechatMessageEntity.getCreateTime());
            return wechatTextMessage;
        }

        @Override
        protected WechatMessageEntity doBackward(WechatTextMessage wechatTextMessage) {
            WechatMessageEntity wechatMessageEntity = new WechatMessageEntity();
            wechatMessageEntity.setFromUserName(wechatTextMessage.getFromUserName());
            wechatMessageEntity.setContent(wechatTextMessage.getMessage());
            wechatMessageEntity.setCreateTime(wechatTextMessage.getSendTime());
            wechatMessageEntity.setMsgId(wechatTextMessage.getMsgId());
            wechatMessageEntity.setMsgType(wechatTextMessage.getMsgType());
            wechatMessageEntity.setToUserName(wechatTextMessage.getToUserName());
            return wechatMessageEntity;
        }
    }

}
