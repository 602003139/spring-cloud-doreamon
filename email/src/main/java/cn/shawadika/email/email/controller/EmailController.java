package cn.shawadika.email.email.controller;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.email.email.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/email")
public class EmailController {

    @Autowired
    private MailService mailService;

    @PostMapping("/securityCode")
    public BaseRestful sendSecurityCode(
            @RequestParam(value = "securityCode")String securityCode,
            @RequestParam(value = "toAddress")String toAddress) throws Exception {
        mailService.sendSecurityCode(securityCode,toAddress);
        return BaseRestful.success();
    }
}
