package cn.shawadika.email.email.service;


import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailService {

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;
    @Value("${spring.mail.username}")
    private String senderEmailAddress;

    /**
     * 发送验证码邮件
     * @param securityCode 验证码
     */
    public void sendSecurityCode(String securityCode,String toEmailAddress) throws Exception {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom(senderEmailAddress);
        helper.setTo(toEmailAddress);
        helper.setSubject("验证码");
        Map<String, String> model = new HashMap<String,String>(){
            {
                put("securityCode", securityCode);
            }
        };
        Template template = freeMarkerConfigurer.getConfiguration().getTemplate("email_template.html");
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        helper.setText(html,true);
        mailSender.send(mimeMessage);
    }
}
