package cn.shawadika.email.email;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.collections.map.HashedMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmailApplicationTests {

	@Autowired
	private JavaMailSender mailSender;
    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;

	@Test
	public void contextLoads() {
	}

	@Test
	public void sendSimpEmail(){
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("shawadika_doraemon@163.com");
		message.setTo("fzlinjianhui@163.com");
		message.setSubject("主题：测试邮件");
		message.setText("测试邮件内容");
        mailSender.send(message);
	}

	@Test
	public void sendTempEmail() throws MessagingException, IOException, TemplateException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("shawadika_doraemon@163.com");
        helper.setTo("fzlinjianhui@163.com");
        helper.setSubject("测试模板邮件");
        Map<String, String> model = new HashMap<String,String>(){
            {
                put("username", "测试名字");
                put("securityCode", "123456");
            }
        };
        Template template = freeMarkerConfigurer.getConfiguration().getTemplate("email_template.html");
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        helper.setText(html,true);
        mailSender.send(mimeMessage);
    }
}




















