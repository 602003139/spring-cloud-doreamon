package cn.shawadika.spider.repo;

import cn.shawadika.common.repo.spider.fz12345.Appeal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppealRepo extends JpaRepository<Appeal,Long>{
}
