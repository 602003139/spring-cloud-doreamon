package cn.shawadika.spider.service;

import cn.shawadika.common.repo.spider.fz12345.Appeal;
import cn.shawadika.spider.repo.AppealRepo;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cn.shawadika.spider.config.Urls.FUZHOU_12345_GOV;


@Service
public class SpiderService {

    @Autowired
    private AppealRepo appealRepo;

    private Document getHtml(String url, String type, Map<String, String> requestParam) throws IOException {
        Connection connect = Jsoup.connect(url);
        Document document;
        if (requestParam != null) {
            requestParam.forEach((key, value) -> connect.data(key, value));
        }
        connect.timeout(3000);
        if (type == null || "get".equalsIgnoreCase(type)) {
            document = connect.get();
        } else if ("post".equalsIgnoreCase(type)) {
            document = connect.post();
        } else {
            document = null;
        }
        return document;
    }

    private Map<String, String> generateConditions(LocalDate startDate, LocalDate endDate, int page, String zoneId, int callStatus) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return new HashMap<String, String>() {
            {
                put("startDate", startDate.format(formatter));
                put("endDate", endDate.format(formatter));
                put("callStatus", String.valueOf(callStatus));
                put("comeFrom", "-1");
                put("callType", "-1");
                put("satisfy", "-1");
                put("searchInt", "1");
                put("pageID", String.valueOf(page));
                put("pageSize", "10");
                put("zoneId", zoneId);
            }
        };
    }

    public int getTotalPages(LocalDate startDate, LocalDate endDate, int page, String zoneId, int callStatus) throws IOException {
        Map<String, String> requestParam = generateConditions(startDate, endDate, page, zoneId, callStatus);
        Document doc = this.getHtml(FUZHOU_12345_GOV, "POST", requestParam);
        int totalPages = 0;
        if (doc != null) {
            System.out.println(doc.body());
            totalPages = Integer.parseInt(doc.getElementsByClass("sumpages").get(0).text());
        }
        return totalPages;
    }

    private List<Appeal> getDataListFor12345(LocalDate startDate, LocalDate endDate, int page, String zoneId, int callStatus) throws IOException {
        Map<String, String> requestParam = generateConditions(startDate, endDate, page, zoneId, callStatus);
        Document doc = this.getHtml(FUZHOU_12345_GOV, "POST", requestParam);
        if (doc != null) {
            Element table = doc.getElementsByClass("row3_2").get(0).getElementsByTag("table").get(0);
            Elements trs = table.getElementsByTag("tr");
            trs.remove(0);//第一个tr是表头,不需要
            return new ArrayList<Appeal>() {
                {
                    Date nowDate = Date.valueOf(LocalDate.now());
                    trs.forEach(tr -> {
                        Appeal a = new Appeal();
                        Elements tds = tr.getElementsByTag("td");
                        a.setAppealNo(tds.get(0).text());
                        a.setAppealType(tds.get(1).text());
                        a.setType(tds.get(2).text());
                        a.setAppealTitle(tds.get(3).getElementsByTag("a").get(0).attr("title"));
                        a.setDealResult(tds.get(4).text());
                        a.setInstructResult(tds.get(5).text());
                        a.setSource(tds.get(6).text());
                        a.setClicks(tds.get(7).text());
                        a.setCreateTime(nowDate);
                        add(a);
                    });
                }
            };
        }
        return null;
    }

    public List<Appeal> buildAppeal(LocalDate startDate, LocalDate endDate, int page, String zoneId, int callStatus) throws IOException {
        List<Appeal> appealList = this.getDataListFor12345(startDate, endDate, page, zoneId, callStatus);
        return appealRepo.save(appealList);
    }

    public void catchFz12345Appeals(int year, LocalDate startDate) throws IOException {
        boolean flag = true;
        //按照时间,地区,状态,分页抓取数据
        LocalDate endDate = startDate.plusDays(7L);
        int totalPages = this.getTotalPages(startDate, endDate, 1, "", -1);
        for (int page = 1; page < totalPages + 1; page++) {
            System.out.println("开始抓取"+startDate+"到"+endDate+"第"+page+"页数据");
            List<Appeal> appealList = this.buildAppeal(startDate, endDate, page, "", -1);
        }
        if (endDate.compareTo(LocalDate.ofYearDay(year, 1)) >= 0) {
            //说明到了下一年了就停止
            flag=false;
        }
        if(flag){
            catchFz12345Appeals(year,endDate.plusDays(1L));
        }
    }

}
