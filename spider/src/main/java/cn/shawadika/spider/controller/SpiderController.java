package cn.shawadika.spider.controller;

import cn.shawadika.spider.service.SpiderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/spider")
public class SpiderController {


    @Autowired
    private SpiderService spiderService;

    private static Map<String, String> areaMap;

    static {
        areaMap = new HashMap<String, String>() {
            {
                put("862", "福州市直属");
                put("788", "鼓楼区");
                put("781", "台江区");
                put("755", "晋安区");
                put("762", "仓山区");
                put("4064", "马尾区");
                put("9869", "福清市");
                put("9876", "长乐区");
                put("9911", "连江县");
                put("9883", "闽侯县");
                put("9904", "闽清县");
                put("9918", "罗源县");
                put("9897", "永泰县");
                put("35674", "高新区");
            }
        };
    }

    public static void main(String[] args) {
        LocalDate L1 = LocalDate.ofYearDay(2017, 3);
        LocalDate L2 = LocalDate.ofYearDay(2017, 3);
        System.out.println(L1.compareTo(L2));

    }
}















