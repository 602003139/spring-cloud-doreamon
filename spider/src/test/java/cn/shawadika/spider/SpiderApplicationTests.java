package cn.shawadika.spider;

import cn.shawadika.spider.service.SpiderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpiderApplicationTests {

	@Autowired
	private SpiderService parserService;

	@Test
	public void contextLoads() throws IOException {
		parserService.catchFz12345Appeals(2017, LocalDate.ofYearDay(2017,1));
	}


}
