layui.define(function (exports) {
    var dhh_table = {
        member_list: [[
            {field: 'account', width: 200, align: 'center', title: "账号", templet: '#iconColumn'},
            {field: 'nickName', width: 120, align: 'center', title: "昵称"},
            {field: 'headImgUrl', width: 80, align: 'center', title: "头像",templet:"#headTemp"},
            {field: 'sex', width: 80, align: 'center', title: "性别",templet:'#sexTemp'},
            {field: 'opera',fixed: 'right', width: 160, align: 'center', toolbar: '#operaBtn', title: "操作"}
        ]],
        wechat_fans_list:[[
            {field: 'nickname', width: 100, align: 'center', title: "昵称"},
            {field: 'sex', width: 60, align: 'center', title: "性别",templet:'#sexTemp'},
            {field: 'headimgurl', width: 60, align: 'center', title: "头像",templet:'#imgTemp'},
            {field: 'address', width: 120, align: 'center', title: "国家",templet:'#addressTemp'},
            {field: 'groupid', width: 100, align: 'center', title: "分组"},
            {field: 'tagid_list', width: 100, align: 'center', title: "标签"},
            {field: 'subscribe_time', width: 150, align: 'center', title: "关注时间",sort:true,templet:'#subscribeTimeTemp'},
            {field: 'openid', width: 200, align: 'center', title: "openid"},
            {field: 'remark', width: 100, align: 'center',title: "备注"},
            {field: 'opera',fixed: 'right', width: 120, align: 'center', toolbar: '#operaBtn',title:'操作'}
        ]],
        request:{
            pageName:"page",
            limitName:"limit"
        },
        response:{
            statusName:"code",
            statusCode:100,
            msgName:"message",
            countName:"totalElements",
            dataName:"data"
        }
    };
    exports("dhh_table", dhh_table);
});