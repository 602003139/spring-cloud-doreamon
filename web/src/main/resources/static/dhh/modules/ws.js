/**
 * 大灰灰
 */
;layui.define(function (exports) {

    var socket={

        webSocket:null,

        websocketAddress:"/sockjs/socketServer",

        doSend:function (socket,message,callback) {
            if(socket && socket.readyState===1){
                socket.send(message);
                callback(true)
            }else{
                callback(false)
            }
        },
        onOpen:function (openEvt) {
           return openEvt
        },
        onMessage:function (msgEvt) {
            return msgEvt;
        },
        onError:function () {

        },
        onClose:function () {

        },
        init:function (address) {
            return socket.open(address);
        },
        open:function (address) {
            address=address?address:socket.websocketAddress;
            socket.websocket = new SockJS(address);
            socket.websocket.onopen = socket.onOpen;
            socket.websocket.onmessage = socket.onMessage;
            socket.websocket.onerror = socket.onError;
            socket.websocket.onclose = socket.onClose;
            return socket.websocket;
        }
    };

    exports('ws',socket);
});