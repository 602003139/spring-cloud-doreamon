;layui.define('jquery',function (exports) {
    var $ = layui.jquery;
    var message={
        wechatMessage:{
            fromUserId:1,
            toUserId:1,
            content:'',
            messageType:'wechat'
        },
        /**
         * 将消息拼接到页面
         */
        appendToPage:function (where,html,data,autoScroll) {
            var $appBlock=$(where);
            var append= Mustache.render(html, data);
            $appBlock.append(append);
            $appBlock.find(".head-img:last").data('userObj',data);
            if(autoScroll){
                //消息自动滚动到底部
                $appBlock.scrollTop($appBlock.prop('scrollHeight'));
            }
        }

    };

    exports('message',message);

});