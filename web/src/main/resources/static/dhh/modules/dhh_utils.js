;layui.define(function (exports) {

    var utils = {
        //获取鼠标点击区域在Html绝对位置坐标
        mouseCoords: function (event) {
            if (event.pageX || event.pageY) {
                return {x: event.pageX, y: event.pageY};
            }
            return {
                x: event.clientX + document.body.scrollLeft - document.body.clientLeft,
                y: event.clientY + document.body.scrollTop - document.body.clientTop
            };
        },
        //判断一个list中是否有某个元素
        hasItem: function (list, item) {
            var result = false;
            for (var i = 0; i < list.length; i++) {
                var _item = list[i];
                if (item === _item) {
                    result = true;
                }
            }
            return result;
        }
    };

    exports('dhh_utils', utils)
});