package cn.shawadika.web.message.handler;

import cn.shawadika.common.repo.member.MemberEntity;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import javax.servlet.http.HttpSession;
import java.util.Map;

import static cn.shawadika.common.constants.MemberConstant.MEMBER_SESSION_KEY;
import static cn.shawadika.common.constants.WebSocketConstant.WEBSOCKET_MEMBER_ID;

public class SpringWebSocketHandlerInterceptor extends HttpSessionHandshakeInterceptor {

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
                                   Map<String, Object> attributes){
        if (request instanceof ServletServerHttpRequest) {
            ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
            HttpSession session = servletRequest.getServletRequest().getSession(true);
            if (session != null) {
                //使用userName区分WebSocketHandler，以便定向发送消息
                MemberEntity memberEntity = (MemberEntity) session.getAttribute(MEMBER_SESSION_KEY);
                if (memberEntity != null) {
                    attributes.put(WEBSOCKET_MEMBER_ID, memberEntity.getId());
                    return true;
                }
            }
        }
//        return super.beforeHandshake(request, response, wsHandler, attributes);

        return false;
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler,
                               Exception ex) {
        super.afterHandshake(request, response, wsHandler, ex);
    }
}
