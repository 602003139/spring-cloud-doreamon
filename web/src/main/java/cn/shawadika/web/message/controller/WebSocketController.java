package cn.shawadika.web.message.controller;


import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.dto.WechatInputMessageDTO;
import cn.shawadika.common.bean.message.WechatTextMessage;
import cn.shawadika.common.repo.wechat.WechatFansInfoEntity;
import cn.shawadika.web.message.handler.SpringWebSocketHandler;
import cn.shawadika.web.wechat.service.ScheduServiceWechat;
import cn.shawadika.web.wechat.service.WechatService;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * https://www.cnblogs.com/3dianpomian/p/5902084.html
 */
@RestController
@RequestMapping("/websocket")
public class WebSocketController {

    @Autowired
    private WechatService wechatService;
    @Autowired
    private SpringWebSocketHandler infoHandler;

    @PostMapping("/gamedailyTextMessage")
    public BaseRestful gamedailyTextMessage(@RequestBody WechatTextMessage wechatTextMessage){
        List<Long> userIds = new ArrayList<Long>(){
            //TODO 这里需要定义接受该消息的用户
            {add(1L);}
        };
        infoHandler.sendWechatTextMessage(userIds,wechatService.supplementWechatUserInfo(wechatTextMessage));
        return BaseRestful.success();
    }


}















