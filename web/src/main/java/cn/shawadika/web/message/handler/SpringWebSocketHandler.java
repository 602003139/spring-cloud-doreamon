package cn.shawadika.web.message.handler;

import cn.shawadika.common.bean.message.MyMessage;
import cn.shawadika.common.bean.message.SystemMessage;
import cn.shawadika.common.bean.message.WechatTextMessage;
import cn.shawadika.common.constants.MessageConstant;
import cn.shawadika.common.constants.WebSocketConstant;
import cn.shawadika.common.factory.MessageFactory;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SpringWebSocketHandler extends TextWebSocketHandler {

    private static final Map<Long, WebSocketSession> users;

    static {
        users = new HashMap<>();
    }

    public SpringWebSocketHandler() {
        // TODO Auto-generated constructor stub
    }

    /**
     * 连接成功时候，会触发页面上onopen方法
     */
    public void afterConnectionEstablished(WebSocketSession session) throws IOException {
        Map<String, Object> map = session.getAttributes();
        Long userId = (Long) map.get(WebSocketConstant.WEBSOCKET_MEMBER_ID);
        users.put(userId, session);
        SystemMessage systemMessage = MessageFactory.createSystemMessage("连接成功", userId);
        TextMessage returnMessage = new TextMessage(JSON.toJSONString(systemMessage));
        session.sendMessage(returnMessage);
    }

    /**
     * 关闭连接时触发
     */
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) {
        System.out.println("websocket connection closed......");
        int currentSize = this.onLineUserSub(session);
        System.out.println("剩余在线用户" + currentSize);
    }

    private int onLineUserSub(WebSocketSession session) {
        long userId = (Long) session.getAttributes().get(WebSocketConstant.WEBSOCKET_MEMBER_ID);
        users.remove(userId);
        return users.size();
    }

    /**
     * js调用websocket.send时候，会调用该方法
     */
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        super.handleTextMessage(session, message);
        String payload = message.getPayload();
        JSONObject jsonObject = JSON.parseObject(payload);
        String type = jsonObject.getString("messageType");
        switch (type) {
            //自己发出来的消息
            case MessageConstant.MY_MESSAGE:

                MyMessage myMessage = JSON.parseObject(payload, MyMessage.class);
                break;
                //微信消息
            case MessageConstant.WECHAT_MESSAGE:
                WechatTextMessage wechatTextMessage = JSON.parseObject(payload, WechatTextMessage.class);
                break;
        }
        long userId = (Long) session.getAttributes().get(WebSocketConstant.WEBSOCKET_MEMBER_ID);
        sendMessageToUser(userId, message);
    }

    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        if (session.isOpen()) {
            session.close();
        }
        System.out.println("websocket connection closed......");
        this.onLineUserSub(session);
    }

    public boolean supportsPartialMessages() {
        return false;
    }

    /**
     * 给某个用户发送消息
     *
     * @param userId
     * @param message
     */
    private void sendMessageToUser(long userId, TextMessage message) throws IOException {
        WebSocketSession webSocketSession = users.get(userId);
        if (webSocketSession != null&&webSocketSession.isOpen()) {
            webSocketSession.sendMessage(message);
        }
    }

    /**
     * 给所有在线用户发送消息
     *
     * @param message
     */
    public void sendMessageToUsers(TextMessage message) throws IOException {

        for (Map.Entry<Long, WebSocketSession> entry : users.entrySet()) {
            WebSocketSession session = entry.getValue();
            if (session.isOpen()) {
                entry.getValue().sendMessage(message);
            }
        }
    }

    public void sendWechatTextMessage(List<Long> userIds, WechatTextMessage message){
        String jsonString = JSON.toJSONString(message);
        TextMessage textMessage = new TextMessage(jsonString);
        userIds.forEach(userId->{
            try {
                this.sendMessageToUser(userId,textMessage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

}
