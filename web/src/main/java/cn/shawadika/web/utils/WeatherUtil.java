package cn.shawadika.web.utils;

import cn.shawadika.common.modules.Weather;
import cn.shawadika.common.util.HttpRequest;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class WeatherUtil {

    private static String WEATHER_NOW_URL=
            "https://api.seniverse.com/v3/weather/now.json?key=v6aroluhrmczkmfp&location=LOCATION&language=zh-Hans&unit=c";

    public static Weather getWeather(String location) throws UnsupportedEncodingException {
        String weather = HttpRequest.sendGet(WEATHER_NOW_URL.replace("LOCATION",URLEncoder.encode(location,"utf-8")),"");
        if(StringUtils.isNotEmpty(weather)){
            JSONObject jsonObject = JSON.parseObject(weather);
            JSONObject results = jsonObject.getJSONArray("results").getJSONObject(0);
            String addressName = results.getJSONObject("location").getString("name");
            JSONObject nowObject = results.getJSONObject("now");
            String weatherDesc = nowObject.getString("text");//晴
            String code = nowObject.getString("code");//9,根据这个代码可以获取天气图标
            String temperature = nowObject.getString("temperature");//温度
            return Weather.builder().addressName(addressName).code(code).weatherDesc(weatherDesc).temperature(temperature).build();
        }
        return null;
    }
}
