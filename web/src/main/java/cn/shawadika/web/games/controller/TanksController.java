package cn.shawadika.web.games.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/games")
public class TanksController {

    @GetMapping("/tanks.html")
    public ModelAndView tanks(ModelAndView view){
        view.setViewName("games/tanks");
        return view;
    }
}
