package cn.shawadika.web.common.handler;


import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.exceptions.BaseException;
import cn.shawadika.common.exceptions.ExceptionCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.lang.reflect.Method;
import java.util.Set;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static final String DEFAULT_ERROR_PAGE="error/error";
    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Object defaultErrorHandler(HttpServletRequest request, Exception e) {
        e.printStackTrace();
        boolean isRestful = false;
        BaseException baseException = null;
        try {
            HandlerMethod handlerMethod = (HandlerMethod) requestMappingHandlerMapping.getHandler(request).getHandler();
            Method method = handlerMethod.getMethod();
            isRestful = method.isAnnotationPresent(ResponseBody.class)
                    || method.getDeclaringClass().isAnnotationPresent(RestController.class);
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        if (e instanceof BaseException)
            baseException = (BaseException) e;
        if (e instanceof ConstraintViolationException) {
            ConstraintViolationException constraintViolationException = (ConstraintViolationException) e;
            Set<ConstraintViolation<?>> violations = constraintViolationException.getConstraintViolations();
            String message = "";
            for (ConstraintViolation<?> violation : violations ) {
                message += violation.getMessage();
                break;
            }
            baseException = new BaseException(ExceptionCode.PARAMS_ERROR, message);
        }
        if (e instanceof BindException) {
            BindException bindException = (BindException) e;
            FieldError fieldError = bindException.getFieldError();
            baseException = new BaseException(ExceptionCode.PARAMS_ERROR, fieldError.getDefaultMessage());
        }
        String code = null == baseException ? ExceptionCode.SYSTEM_ERROR : baseException.getCode();
        String message = null == baseException ? e.getMessage() : baseException.getMessage();
        if (isRestful) {
            return new BaseRestful(code, message,null);
        } else {
            ModelAndView mav = new ModelAndView();
            mav.addObject("message", message);
            mav.addObject("code", code);
            mav.setViewName(DEFAULT_ERROR_PAGE);
            return mav;
        }
    }
}
