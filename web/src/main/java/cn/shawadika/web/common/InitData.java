package cn.shawadika.web.common;

import cn.shawadika.common.repo.navigation.Navigation;
import cn.shawadika.common.repo.navigation.TopMenu;
import cn.shawadika.web.navigation.NavigationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Component
public class InitData {

    @Autowired
    private NavigationService navigationService;
    @Value("${doraemon.copyright}")
    private String copyright;

    @Bean
    public ServletContextInitializer initNavigationData() throws IOException {
        Navigation navigation = navigationService.getNavigation();
        List<TopMenu> menuList = navigation.getMenuList();
        return (servletContext) ->{
            servletContext.setAttribute("leftMenuList", menuList.get(0).getChild());
            servletContext.setAttribute("topMenuList", menuList);
            servletContext.setAttribute("year", LocalDate.now().getYear());
            servletContext.setAttribute("copyright", copyright);
        };
    }
}
