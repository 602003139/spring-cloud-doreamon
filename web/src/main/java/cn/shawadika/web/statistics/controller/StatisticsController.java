package cn.shawadika.web.statistics.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/statistics")
public class StatisticsController {

    @GetMapping("/index.html")
    public ModelAndView index(ModelAndView view){
        view.setViewName("statistics/index");
        return view;
    }
}
