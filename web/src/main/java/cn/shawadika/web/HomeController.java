package cn.shawadika.web;

import cn.shawadika.common.repo.navigation.TopMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import java.util.List;


@Controller("web_home_controller")
public class HomeController {

    @Autowired
    private ServletContext servletContext;

    @GetMapping("/")
    public ModelAndView home(ModelAndView view, @RequestParam(value = "sort",defaultValue = "0") Integer sort) {
        List<TopMenu> menuList = (List<TopMenu>) servletContext.getAttribute("topMenuList");
        if (menuList.size() > sort) {
            servletContext.setAttribute("leftMenuList", menuList.get(sort).getChild());
        }
        view.setViewName("index/index");
        return view;
    }

}
