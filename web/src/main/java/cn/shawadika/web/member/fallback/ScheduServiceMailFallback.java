package cn.shawadika.web.member.fallback;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.web.member.service.ScheduServiceMail;
import org.springframework.stereotype.Component;

import static cn.shawadika.common.Restful.BaseRestful.SYSTEM_ERROR_CODE;

@Component
public class ScheduServiceMailFallback implements ScheduServiceMail {
    @Override
    public BaseRestful sendSecurity(String securityCode, String toAddress) {
        return BaseRestful.builder().message("服务器调用异常").code(SYSTEM_ERROR_CODE).build();
    }
}
