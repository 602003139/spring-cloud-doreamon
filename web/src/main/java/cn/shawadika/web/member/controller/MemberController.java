package cn.shawadika.web.member.controller;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.Restful.PageRestful;
import cn.shawadika.common.dto.ArithmeticDTO;
import cn.shawadika.common.dto.MemberDTO;
import cn.shawadika.common.dto.PageDTO;
import cn.shawadika.common.repo.member.MemberEntity;
import cn.shawadika.common.util.ArithmeticUtil;
import cn.shawadika.common.validator.Password;
import cn.shawadika.web.member.service.ScheduServiceMail;
import cn.shawadika.web.member.service.ScheduServiceMember;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Random;

import static cn.shawadika.common.constants.MemberConstant.*;

@Controller("web_member_controller")
@RequestMapping("/member")
@Validated
public class MemberController {

    @Autowired
    private ScheduServiceMember scheduServiceMember;
    @Autowired
    private ScheduServiceMail scheduServiceMail;

    @GetMapping("/login.html")
    public ModelAndView login(ModelAndView view, HttpSession session) {
        view.setViewName("member/login");
        //生成人类验证题目
        ArithmeticDTO arithmetic = ArithmeticUtil.generateArithmetic();
        view.addObject("question",arithmetic.getQuestion());
        session.setAttribute(MEMBER_LOGIN_ARITHMETIC_RESULT_KEY,arithmetic.getResult());
        return view;
    }

    @GetMapping("/regist.html")
    public ModelAndView regist(ModelAndView view,HttpSession session) {
        view.setViewName("member/regist");
        //生成人类验证题目
        ArithmeticDTO arithmetic = ArithmeticUtil.generateArithmetic();
        view.addObject("question",arithmetic.getQuestion());
        session.setAttribute(MEMBER_REGIST_ARITHMETIC_RESULT_KEY,arithmetic.getResult());
        return view;
    }

    @GetMapping("/forget.html")
    public ModelAndView forget(ModelAndView view) {
        view.setViewName("member/forget");
        return view;
    }

    @PostMapping("/sendSecurityCode")
    @ResponseBody
    public BaseRestful sendSecurityCode(@Email String email,HttpSession session) {
        String securityCode = this.generateSecurityCode();
        BaseRestful result = scheduServiceMail.sendSecurity(securityCode, email);
        if(result.isSuccess()){
            session.setAttribute(MEMBER_REGIST_SECURITY_CODE_KEY,securityCode);
        }
        return result;
    }

    @PostMapping("/login")
    @ResponseBody
    public BaseRestful login(@NotEmpty String password,@NotEmpty String account,@NotEmpty String vercode, HttpSession session) {
        if(!vercode.equals(session.getAttribute(MEMBER_LOGIN_ARITHMETIC_RESULT_KEY))){
            return BaseRestful.error("人类验证失败！");
        }
        MemberEntity member = scheduServiceMember.findByAccountAndPassword(account, password);
        if (member != null) {
            this.saveSession(session, member);
            return BaseRestful.success();
        }
        return BaseRestful.error("账号或密码错误");
    }

    @GetMapping("/exit")
    public void exit(HttpSession session, HttpServletResponse response) throws IOException {
        session.removeAttribute(MEMBER_SESSION_KEY);
        response.sendRedirect("/");
    }

    @PostMapping("/regist")
    @ResponseBody
    public BaseRestful regist(@Email String account,
                              @Password String password,
                              @NotEmpty String securityCode,
                              @NotEmpty String vercode,
                              HttpSession session) {
        if(!securityCode.equalsIgnoreCase((String)session.getAttribute(MEMBER_REGIST_SECURITY_CODE_KEY))){
            return BaseRestful.error("验证码错误");
        }
        if(!vercode.equals(session.getAttribute(MEMBER_REGIST_ARITHMETIC_RESULT_KEY))){
            return BaseRestful.error("人类验证失败！");
        }
        return scheduServiceMember.buildMember(account, password);
    }

    @GetMapping("/list")
    @ResponseBody
    public PageRestful memberList(@RequestParam(name = "page", defaultValue = "0") int page,
                                  @RequestParam(name = "limit", defaultValue = "10") int limit) {
        PageDTO<MemberDTO> entityPage = scheduServiceMember.findAll(page, limit);
        if(entityPage!=null){
           return PageRestful.success(entityPage);
        }
        return PageRestful.error("数据列表为空");
    }

    @GetMapping("/list.html")
    public ModelAndView list(ModelAndView view) {
        view.setViewName("member/list");
        return view;
    }

    private void saveSession(HttpSession session, MemberEntity member) {
        member.setPassword("");
        member.setEncrypt("");
        session.setAttribute(MEMBER_SESSION_KEY, member);
    }

    private String generateSecurityCode() {
        StringBuilder securityCode=new StringBuilder("");
        String str = "0,1,2,3,4,5,6,7,8,9";
        String[] split = str.split(",");
        Random random = new Random();
        for (int i = 0; i < MAX_SECURITY_CODE_LENGTH; i++) {
            securityCode.append(split[random.nextInt(split.length)]);
        }
        return securityCode.toString();
    }

}
