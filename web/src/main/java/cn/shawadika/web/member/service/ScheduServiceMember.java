package cn.shawadika.web.member.service;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.dto.MemberDTO;
import cn.shawadika.common.dto.PageDTO;
import cn.shawadika.common.repo.member.MemberEntity;
import cn.shawadika.web.member.fallback.ScheduServiceMemberFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "service-member",fallback = ScheduServiceMemberFallback.class)
public interface ScheduServiceMember {

    @GetMapping("/member/find")
    MemberEntity findByAccountAndPassword(@RequestParam(name = "account") String account,
                                          @RequestParam(name = "password") String password);

    @PostMapping("/member/build")
    BaseRestful buildMember(@RequestParam(name = "account") String account,
                            @RequestParam(name = "password") String password);

    @GetMapping("/member/list")
    PageDTO<MemberDTO> findAll(@RequestParam(name = "page") int page,
                               @RequestParam(name = "limit") int limit);
}
