package cn.shawadika.web.member.fallback;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.dto.MemberDTO;
import cn.shawadika.common.dto.PageDTO;
import cn.shawadika.common.repo.member.MemberEntity;
import cn.shawadika.web.member.service.ScheduServiceMember;
import org.springframework.stereotype.Component;

import static cn.shawadika.common.Restful.BaseRestful.SYSTEM_ERROR_CODE;

@Component
public class ScheduServiceMemberFallback implements ScheduServiceMember {
    @Override
    public MemberEntity findByAccountAndPassword(String account, String password) {
        return null;
    }

    @Override
    public BaseRestful buildMember(String account, String password) {
        return BaseRestful.builder().code(SYSTEM_ERROR_CODE).message("系统异常").build();
    }

    @Override
    public PageDTO<MemberDTO> findAll(int page, int limit) {
        return null;
    }
}
