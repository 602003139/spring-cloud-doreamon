package cn.shawadika.web.member.service;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.web.member.fallback.ScheduServiceMailFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "service-email",fallback = ScheduServiceMailFallback.class)
public interface ScheduServiceMail {
    
    @PostMapping("/email/securityCode")
    BaseRestful sendSecurity(
            @RequestParam(value = "securityCode")String securityCode,
            @RequestParam(value = "toAddress")String toAddress);
}
