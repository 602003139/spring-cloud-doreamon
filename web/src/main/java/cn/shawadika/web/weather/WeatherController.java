package cn.shawadika.web.weather;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.modules.Weather;
import cn.shawadika.common.util.CusAccessObjectUtil;
import cn.shawadika.web.utils.WeatherUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("/weather")
public class WeatherController {

    @GetMapping("/getWeather")
    public BaseRestful getWeather(HttpServletRequest request) throws UnsupportedEncodingException {
        String ipAddress = CusAccessObjectUtil.getIpAddress(request);
        if(StringUtils.isNotEmpty(ipAddress)){
            if(!"127.0.0.1".equals(ipAddress) && !"0:0:0:0:0:0:0:1".equals(ipAddress)){
                Weather weather = WeatherUtil.getWeather(ipAddress);
                return BaseRestful.success(weather);
            }
        }
        return BaseRestful.success(WeatherUtil.getWeather("福州"));
    }
}
