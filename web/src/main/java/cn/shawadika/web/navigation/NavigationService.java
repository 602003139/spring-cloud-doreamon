package cn.shawadika.web.navigation;

import cn.shawadika.common.repo.navigation.Navigation;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Service
public class NavigationService {

    public Navigation getNavigation() throws IOException {

//        File navigation = ResourceUtils.getFile("classpath:navigation.json");

        //这里读取json需要这么读取，不然在打成jar包后会无法读取
        //参考：http://blog.csdn.net/liu251/article/details/4057140

        InputStream inputStream = this.getClass().getResourceAsStream("/navigation.json");

        return new ObjectMapper().readValue(inputStream,Navigation.class);
    }
}
