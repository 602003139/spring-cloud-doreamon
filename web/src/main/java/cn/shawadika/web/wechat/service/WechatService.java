package cn.shawadika.web.wechat.service;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.bean.message.WechatTextMessage;
import cn.shawadika.common.repo.wechat.WechatFansInfoEntity;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("web_wechat_service")
public class WechatService {

    @Autowired
    private ScheduServiceWechat scheduServiceWechat;

    /**
     * 补充页面需要显示的微信用户的一些必要信息
     * @param wechatTextMessage
     */
    public WechatTextMessage supplementWechatUserInfo(WechatTextMessage wechatTextMessage){
        //TODO 这里需要知道这个消息是发给哪个公众号的
        BaseRestful info = scheduServiceWechat.getWechatInfo(1L, wechatTextMessage.getFromUserName());
        if(info.isSuccess()){
            String fansInfoStr = (String) info.getData();
            WechatFansInfoEntity fansInfo = JSON.parseObject(fansInfoStr, WechatFansInfoEntity.class);
            if(fansInfo!=null){
                wechatTextMessage.setWechatFansImgUrl(fansInfo.getHeadimgurl());
                wechatTextMessage.setWechatFansNickName(fansInfo.getNickname());
            }
        }
        return wechatTextMessage;
    }
}
