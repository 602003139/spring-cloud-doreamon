package cn.shawadika.web.wechat.service;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.dto.PageDTO;
import cn.shawadika.common.repo.wechat.WechatFansInfoEntity;
import cn.shawadika.common.repo.wechat.WechatMessageEntity;
import cn.shawadika.web.wechat.fallback.ScheduServiceWechatFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "service-wechat",fallback = ScheduServiceWechatFallback.class)
public interface ScheduServiceWechat {

    @GetMapping("/wechat/fans")
    PageDTO<WechatFansInfoEntity> findList(@RequestParam(value = "page") int page, @RequestParam(value = "limit") int limit);

    @PostMapping("/wechat/build_openid")
    void updateOpenid(@RequestParam(value = "gamedailyId") long gamedailyId);

    @PostMapping("/wechat/build_fans")
    BaseRestful updateFansInfo(@RequestParam(value = "gamedailyId") long gamedailyId);

    @GetMapping("/wechat/message/all/{gamedailyId}")
    PageDTO<WechatMessageEntity> findAllMessage(
            @PathVariable("gamedailyId") long gamedailyId,
            @RequestParam(value = "page") int page,
            @RequestParam(value = "limit") int limit,
            @RequestParam(value = "openId") String openId);

    @GetMapping("/wechat/fans/{gamedailyID}/{openid}")
    BaseRestful getWechatInfo(@PathVariable("gamedailyID") long gamedailyID, @PathVariable("openid") String openid);
}
