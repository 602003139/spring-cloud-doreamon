package cn.shawadika.web.wechat.fallback;

import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.dto.PageDTO;
import cn.shawadika.common.exceptions.BaseException;
import cn.shawadika.common.exceptions.ExceptionCode;
import cn.shawadika.common.repo.wechat.WechatFansInfoEntity;
import cn.shawadika.common.repo.wechat.WechatMessageEntity;
import cn.shawadika.web.wechat.service.ScheduServiceWechat;
import org.springframework.stereotype.Component;

@Component
public class ScheduServiceWechatFallback implements ScheduServiceWechat{

    @Override
    public PageDTO<WechatFansInfoEntity> findList(int page, int limit) {
        return null;
    }

    @Override
    public void updateOpenid(long gamedailyId) {

    }

    @Override
    public BaseRestful updateFansInfo(long gamedailyId) {
        return null;
    }

    @Override
    public PageDTO<WechatMessageEntity> findAllMessage(long gamedailyId, int page, int limit, String openId) {
        throw new BaseException(ExceptionCode.SYSTEM_ERROR, "服务器调用异常");
    }

    public BaseRestful getWechatInfo(long gamedailyID, String openid) {
        return BaseRestful.error("服务器调用异常");
    }
}
