package cn.shawadika.web.wechat.controller;


import cn.shawadika.common.Restful.BaseRestful;
import cn.shawadika.common.Restful.PageRestful;
import cn.shawadika.common.bean.message.BaseMessage;
import cn.shawadika.common.bean.message.WechatTextMessage;
import cn.shawadika.common.constants.WechatMessageTypeConstant;
import cn.shawadika.common.dto.PageDTO;
import cn.shawadika.common.repo.wechat.WechatFansInfoEntity;
import cn.shawadika.common.repo.wechat.WechatMessageEntity;
import cn.shawadika.common.validator.Auth;
import cn.shawadika.web.wechat.service.ScheduServiceWechat;
import cn.shawadika.web.wechat.service.WechatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

import static com.sun.org.apache.xml.internal.serializer.utils.Utils.messages;

@Controller("web_wechat_controller")
@RequestMapping("/wechat")
//@Auth
public class WechatController {

    @Autowired
    private ScheduServiceWechat scheduServiceWechat;
    @Autowired
    private WechatService wechatService;

    @GetMapping("/fanslist.html")
    public ModelAndView index(ModelAndView view) {
        view.setViewName("wechat/fanslist");
        return view;
    }

    @GetMapping("/message.html")
    public ModelAndView message(ModelAndView view) {
        view.setViewName("wechat/message");
        return view;
    }

    @GetMapping("/fanslist")
    @ResponseBody
    public PageRestful list(@RequestParam(value = "page") int page,
                            @RequestParam(value = "limit") int limit) {
        PageDTO<WechatFansInfoEntity> list = scheduServiceWechat.findList(page, limit);
        if (list != null) {
            return PageRestful.success(list);
        }
        return PageRestful.error("数据列表为空");
    }

    @GetMapping("/message/all")
    @ResponseBody
    public PageRestful messageList(
            @RequestParam(value = "page", defaultValue = "1") int page,
            @RequestParam(value = "limit", defaultValue = "10") int limit,
            @RequestParam(value = "openId",defaultValue = "") String openId) {
        //TODO 这里的公众号id需要从登录者session中获取
        PageDTO<WechatMessageEntity> message = scheduServiceWechat.findAllMessage(1L, page, limit,openId);
        List<WechatMessageEntity> wechatMessageList = message.getData();
        List<BaseMessage> messageList = new ArrayList<BaseMessage>() {
            {
                wechatMessageList.forEach(msg -> {
                    switch (msg.getMsgType()) {
                        //文本消息
                        case WechatMessageTypeConstant.TEXT_MESSAGE:
                            add(wechatService.supplementWechatUserInfo(WechatTextMessage.convertBack(msg)));
                            break;
                        //其他形式的消息还未完善
                        //...
                    }
                });
            }
        };
        return PageRestful.success(new PageDTO().covertToPageDTO(message,messageList));
    }

    @GetMapping("/refresh")
    @ResponseBody
    public BaseRestful refreshFansList() {
        //TODO 这里的gamedailyId需要动态获取
        //更新openid
        scheduServiceWechat.updateOpenid(1L);
        //更新粉丝信息
        return scheduServiceWechat.updateFansInfo(1L);
    }

}
